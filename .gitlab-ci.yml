workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED == "true"

include: 
  - local: tests/unit.gitlab-ci.yml
    rules:
      - changes:
          - src/gitlab-tofu.sh
          - Dockerfile
          - .gitlab-ci.yml
          - tests/unit.gitlab-ci.yml
      - if: $CI_COMMIT_TAG
  - local: tests/integration.gitlab-ci.yml
    rules:
      - changes:
          - src/gitlab-tofu.sh
          - Dockerfile
          - .gitlab-ci.yml
          - templates/*.yml
          - tests/integration.gitlab-ci.yml
          - tests/integration-tests/*.yml
          - tests/iac/**.tf
          - backports/*.gitlab-ci.yml
          - backports/OpenTofu/*.gitlab-ciyml
      - if: $CI_COMMIT_TAG
  # FIXME: we cannot make this work for all use cases because of the following:
  # - cannot pass parallel.matrix to the component, thus we need to extend it
  # - that leads to a problem that when this component is ruled out, the extended job fails, 
  #   because it doesn't have a script or trigger.
  #   Additionally, when this project is mirrored into another instance the component reference fails.
  #   This may be solved with https://gitlab.com/gitlab-org/gitlab/-/issues/434260#note_1776822074

  # - component: gitlab.com/components/container-scanning/container-scanning@3.0
  #   inputs:
  #     stage: quality
  #     cs_image: $GITLAB_OPENTOFU_IMAGE_NAME
  #     git_strategy: fetch

stages:
  - build
  - test
  - test-integration
  - quality
  - deploy
  - release
  - cleanup

.opentofu-versions:
  parallel:
    matrix:
      - OPENTOFU_VERSION: '1.7.0-alpha1'
      - OPENTOFU_VERSION: '1.6.2'
      - OPENTOFU_VERSION: '1.6.1'
      - OPENTOFU_VERSION: '1.6.0'

variables:
  # Pipeline configuration
  DOCKER_DIND_IMAGE: "docker:26.0.2-dind"

  # OpenTofu variables
  LATEST_OPENTOFU_VERSION: '1.6.2'

  # OpenTofu image build variables:
  PLATFORMS: linux/amd64,linux/arm64
  BASE_IMAGE: "alpine:3.19.1"
  GITLAB_OPENTOFU_IMAGE_BASE: "$CI_REGISTRY_IMAGE/internal"
  GITLAB_OPENTOFU_IMAGE_NAME: "$GITLAB_OPENTOFU_IMAGE_BASE/gitlab-opentofu:$CI_COMMIT_SHA-opentofu$OPENTOFU_VERSION"

check-semantic-version:
  stage: .pre
  rules:
    - if: $CI_COMMIT_TAG
  image: alpine:latest
  before_script:
    - apk add perl
  script:
    - echo -n "$CI_COMMIT_TAG" | ./.gitlab/scripts/check-semantic-version.sh

gitlab-opentofu-image:build:
  extends: .opentofu-versions
  stage: build
  services:
    - "$DOCKER_DIND_IMAGE"
  image: "$DOCKER_DIND_IMAGE"
  before_script:
    - apk add git
    # See note on the `build terraform` job about this image
    - docker run --rm --privileged tonistiigi/binfmt
    # Registry auth
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker buildx create --use
    # NOTE: we disable provenance for now
    # because it causes troubles with the registry and older clients.
    # See
    # - https://gitlab.com/gitlab-org/terraform-images/-/issues/104
    # - https://gitlab.com/gitlab-org/terraform-images/-/merge_requests/184#note_1328485943
    - docker buildx build
      --platform "$PLATFORMS"
      --build-arg BASE_IMAGE=$BASE_IMAGE
      --build-arg OPENTOFU_VERSION=$OPENTOFU_VERSION
      --file Dockerfile
      --tag "$GITLAB_OPENTOFU_IMAGE_NAME"
      --provenance=false
      --push
      .
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
        - Dockerfile
        - .gitlab-ci.yml
        - src/**/*
        - templates/**/*
        - tests/**/*
        - backports/**/*

check-readme:
  stage: test
  needs: []
  image: alpine:latest
  before_script:
    - apk add coreutils yq make git
  script:
    - make docs
    - git diff --exit-code
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
        - Makefile
        - .gitlab-ci.yml
        - README.md
        - .gitlab/README.md.template
        - templates/**/*

check-backports:
  stage: test
  needs: []
  image: alpine:latest
  before_script:
    - apk add coreutils make git sed yq diffutils patch
  script:
    - make backports
    - git diff --exit-code
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
        - Makefile
        - .gitlab-ci.yml
        - backports/**/*
        - templates/**/*

shellcheck:
  stage: test
  needs: []
  image: koalaman/shellcheck-alpine:v0.10.0
  script:
    - shellcheck ./src/gitlab-tofu.sh
    - shellcheck ./.gitlab/scripts/*.sh
  rules:
    - changes:
        - .gitlab-ci.yml
        - src/gitlab-tofu.sh
        - .gitlab/scripts/*.sh
    - if: $CI_COMMIT_TAG

# FIXME: see component include why we have to disable this.
# container_scanning:
#   extends: .opentofu-versions
#   rules:
#     - changes:
#         - src/gitlab-tofu.sh
#         - Dockerfile
#         - .gitlab-ci.yml
#     - if: $CI_COMMIT_TAG
#     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

gitlab-opentofu-image:deploy:with-opentofu-version:
  extends: .opentofu-versions
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  variables:
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/gitlab-opentofu"
    # OCI image tags are not compatible with semver, specifically the build metadata part 
    # indicated with a `+` sign, see https://github.com/distribution/distribution/issues/1201
    # We use a dash `-` here, instead of the `+`.
    # This may be problematic, because it indicates a semver prerelease.
    RELEASE_SEMVER: "${CI_COMMIT_TAG}-opentofu${OPENTOFU_VERSION}"
    RELEASE_IMAGE: "$RELEASE_IMAGE_NAME:$RELEASE_SEMVER"
  before_script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - crane copy "$GITLAB_OPENTOFU_IMAGE_NAME" "$RELEASE_IMAGE"
    - 'echo "- \`$RELEASE_IMAGE\` (digest: \`$(crane digest $RELEASE_IMAGE)\`)" > image$CI_JOB_ID.md'
  artifacts:
    paths:
      - 'image*.md'
  rules:
    - if: $CI_COMMIT_TAG

gitlab-opentofu-image:deploy:latest-with-opentofu-version:
  extends: .opentofu-versions
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  variables:
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/gitlab-opentofu"
    RELEASE_SEMVER: "latest-opentofu${OPENTOFU_VERSION}"
    RELEASE_IMAGE: "$RELEASE_IMAGE_NAME:$RELEASE_SEMVER"
  before_script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - crane copy "$GITLAB_OPENTOFU_IMAGE_NAME" "$RELEASE_IMAGE"
    - 'echo "- \`$RELEASE_IMAGE\` (digest: \`$(crane digest $RELEASE_IMAGE)\`)" > image$CI_JOB_ID.md'
  artifacts:
    paths:
      - 'image*.md'
  rules:
    - if: $CI_COMMIT_TAG

gitlab-opentofu-image:deploy:latest:
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  variables:
    OPENTOFU_VERSION: $LATEST_OPENTOFU_VERSION
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/gitlab-opentofu"
    RELEASE_IMAGE: "$RELEASE_IMAGE_NAME:$RELEASE_IMAGE_TAG"
  before_script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - crane copy "$GITLAB_OPENTOFU_IMAGE_NAME" "$RELEASE_IMAGE"
    - 'echo "- \`$RELEASE_IMAGE\` (digest: \`$(crane digest $RELEASE_IMAGE)\`)" > image$CI_JOB_ID.md'
  artifacts:
    paths:
      - 'image*.md'
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix:
      - RELEASE_IMAGE_TAG: ${CI_COMMIT_TAG}
      - RELEASE_IMAGE_TAG: ${CI_COMMIT_TAG}-opentofulatest
      - RELEASE_IMAGE_TAG: latest-opentofulatest
      - RELEASE_IMAGE_TAG: latest

# If the pipeline is for a new tag with a semantic version, and all previous jobs succeed,
# create the release.
create-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add --update yq envsubst
  script: 
    - echo "Creating release $CI_COMMIT_TAG"
    - AVAILABLE_OPENTOFU_VERSIONS=$(yq -r '.spec.inputs.opentofu_version.options | filter((. | test("\$.*")) == false) | .[] | "- [`" + . + "`](https://github.com/opentofu/opentofu/releases/tag/v" + . + ")"' templates/full-pipeline.yml)
    - export AVAILABLE_OPENTOFU_VERSIONS
    - 'AVAILABLE_IMAGES=$(cat image*.md | sort -r | tee images.md | sed -E "s/(\(digest: .*\))/\n  - \1/")'
    - export AVAILABLE_IMAGES
    - cat .gitlab/release-notes.md.template | envsubst > release-notes.md 
  artifacts:
    paths:
      - images.md
      - release-notes.md
  release:
    tag_name: $CI_COMMIT_TAG
    description: './release-notes.md'

old-states:
  image: alpine:latest
  stage: cleanup
  variables:
    REMOVE_STATES_UNTIL: 1 week ago
    GITLAB_TOKEN: $GITLAB_STATE_CLEANER_TOKEN
  before_script:
    - apk add --update coreutils curl jq
    - export FETCH_OLDER_THAN=$(date '+%Y-%m-%dT%H:%M:%SZ' -d "${REMOVE_STATES_UNTIL}")
  script:
    - echo "Going to remove Terraform States older than '$FETCH_OLDER_THAN'"
    - ./.gitlab/scripts/fetch-states.sh | sed -n '1d;p' | ./.gitlab/scripts/remove-states.sh
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "schedule" && $STATE_CLEANER == "true"'
